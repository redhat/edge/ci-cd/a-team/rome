import pytest


@pytest.fixture
def nvrs() -> list:
    return [
        "vim-common-8.2.2637-15.el9",
        "NetworkManager-1.37.2-1.el9",
        "NetworkManager-libnm-1.37.2-1.el9",
    ]


@pytest.fixture
def submit_package_vars() -> list:
    return [
        "vim-common-8.2.2637-15.el9",
        "vim-common-8.2.2637-15.el9",
        "dover_submit_endpoint",
    ]


@pytest.fixture
def json_file_data() -> str:
    return '{"cs9":{"common":["qemu-img-6.2.0-11.el9"],\
"arch":{"aarch64":["grub2-efi-aa64-2.06-25.el9"],\
"x86_64":["glibc-headers-2.34-25.el9"]}}}'


@pytest.fixture
def message_body() -> str:
    return '"body": { \
                    "artifact": { \
                        "component": "qemu-img", \
                        "scratch": "false", \
                        "id": "476994", \
                        "build_id": 17482, \
                        "nvr": "qemu-img-6.2.0-11.el9", \
                        "type": "koji-build", \
                        "issuer": "deamn"} \
                    } \
            "topic": "test"'
